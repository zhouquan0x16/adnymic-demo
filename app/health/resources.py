import random

from flask import Blueprint, jsonify, make_response
from flask_restful import Api, Resource

health_bp = Blueprint('health_api', __name__)
api = Api(health_bp)

class Health(Resource):
    def get(self):
        """
        GET /api/health/

        :return:

        """
        try:
            # Set the 25% rate to fail
            if random.randint(0, 3) == 3:
                raise Exception
            return make_response(jsonify({"message": "running"}), 200)
        except Exception as e:
            return make_response(jsonify({"message": "failed"}), 400)

api.add_resource(Health, '')
