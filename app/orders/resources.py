from flask import Flask, request, jsonify, Blueprint, make_response

from flask_restful import Api, Resource
from marshmallow import Schema, fields

from app import db
from app.orders.models import Order

def check_int_custom_length(length):
    def check_int_length(number):
        return isinstance(number, int) and len(str(number)) == length
    return check_int_length

class OrderSchema(Schema):
    order_id = fields.Str(required=True)
    products = fields.List(fields.Str())
    customer_id = fields.Int(equired=True, validate=check_int_custom_length(8))
    customer_name = fields.Str(required=True)
    customer_surname = fields.Str(required=True)
    customer_address = fields.Str(required=True)
    customer_zipcode = fields.Int(required=True)

orders_bp = Blueprint('orders_api', __name__)
api = Api(orders_bp)

order_schema = OrderSchema()

class Orders(Resource):
    def post(self):
        """
        POST /api/orders/

        :return: json object
        """
        try:
            json_data = request.get_json()
            if not json_data:
                return make_response(jsonify({"message": "No input data provided"}), 400)

            # Validate and deserialize input
            data, errors = order_schema.load(json_data)
            if errors:
                message = ", ".join(
                    ["incorrect value: <{}:{}>".format(str(k), str(v)) for k, v in errors.items()])
                return make_response(jsonify({"message": message}), 400)

            # Create new order
            order = Order(
                order_id = data['order_id'],
                products = data['products'],
                customer_id = data['customer_id'],
                customer_name = data['customer_name'],
                customer_surname = data['customer_surname'],
                customer_address = data['customer_address'],
                customer_zipcode = data['customer_zipcode'],
            )
            order.save()
            result = order_schema.dump(Order.query.get(order.order_id))
            return make_response(jsonify(**result.data), 201)
        except Exception as e:
            print(e)
            # TODO LOGGGING
            return make_response(jsonify({"message": "internal server error"}), 500)


class OrderDetail(Resource):

    def get(self, customer_id):
        """
        GET /api/orders/<customer_id>

        :return: json object
        """
        try:
            # Validate and deserialize input
            if not (isinstance(customer_id, int) and len(str(customer_id)) == 8):
                return make_response(
                    jsonify({"message": "incorrect value: <{}:{}>".format("customer_id", customer_id)}), 400)
            # search the order based on customer_id
            order = Order.query.filter_by(customer_id=customer_id).first()
            if not order:
                return make_response(
                    jsonify({"message": "Not found: <{}:{}>".format("customer_id", customer_id)}), 404)
            result = order_schema.dump(order)
            return make_response(
                jsonify(**result.data), 200)
        except Exception as e:
            # TODO LOGGGING
            print(e)
            return make_response(jsonify({"message": "internal server error"}), 500)

api.add_resource(OrderDetail, '/<int:customer_id>')
api.add_resource(Orders, '')
