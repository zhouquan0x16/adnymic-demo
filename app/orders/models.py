from app import db

class Order(db.Model):

    __tablename__ = 'orders'

    order_id = db.Column(db.String, primary_key=True)
    products = db.Column(db.ARRAY(db.String()))
    customer_id = db.Column(db.Integer, nullable=False)
    customer_name = db.Column(db.String(255), nullable=False)
    customer_surname = db.Column(db.String(255), nullable=False)
    customer_address = db.Column(db.String(255), nullable=False)
    customer_zipcode = db.Column(db.Integer, nullable=False)

    def __init__(self, **kwargs):
        for field, value in kwargs.items():
            setattr(self, field, value)

    def __repr__(self):
        return '<order %r>' % (self.order_id)

    def save(self):
        db.session.add(self)
        db.session.commit()
