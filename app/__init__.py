import flask

from flask_restful import Api
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.migrate import Migrate

app = flask.Flask(__name__)

api = Api(app)

app.config.from_object('config')
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# Ensure db, app is created
from app.health.resources import health_bp
from app.orders.resources import orders_bp

app.register_blueprint(
    health_bp,
    url_prefix='/api/health'
)

app.register_blueprint(
    orders_bp,
    url_prefix='/api/orders'
)
