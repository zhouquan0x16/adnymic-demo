### Flask Sample Restful API Web application

### Intall the requirements
  `pip install -r requirements.txt`

#### Database initialization and setup
  To Create the Schema and the Tables in this demo, you need first run:
  First to initlize the migration support for the application:

  `python manage.py db init`

  Then migrate the Models in the application:

  `python manage.py db migrate`

  `python manage.py db upgrade`

  During the upgrade, there would be an error raise as follow:
  ```
  sa.Column('products', sa.ARRAY(String()), nullable=True),
  NameError: global name 'String' is not defined
  ```
  This issue is caused by [alembic](https://github.com/zzzeek/alembic/pull/38/files)
  There are serval way to solve this issue:
  1. Manually change new generated file in `migration/versions`
    Change the line

    `sa.Column('products', sa.ARRAY(String()), nullable=True),`

    To

    `sa.Column('products', sa.ARRAY(sa.String()), nullable=True),`

  2. Dedicated test database for test, but if we change the db model or have changes in ,
      we still have to change the file manually.

  3. Change redefined the products type to `ARRAY OF Integer`, since it is supported by the alembic

#### Run Test
  Currently the test loads the same configuration for Database, so it will clean everything in the database
  In the future we can create a database only for testing. To test, just run:

  `python manage.py test`

#### Run the application
  To run it, simply run:

  `python manage.py runserver`
