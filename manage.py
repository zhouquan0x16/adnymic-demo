#!/usr/bin/env python
import unittest

from app import app, db
from flask.ext.script import Manager, Server
from flask.ext.migrate import MigrateCommand

manager = Manager(app)
manager.add_command('runserver', Server())
manager.add_command('db', MigrateCommand)

# define our command for testing called "test"
# Usage: python manage.py test
@manager.command
def test():
    """Runs the unit tests without test coverage."""
    tests = unittest.TestLoader().discover('./tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

if __name__ == '__main__':
    manager.run()
