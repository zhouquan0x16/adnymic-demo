import os

# Flask Core Settings
APP_NAME   = "Demo"
DEBUG      = True
HOST       = 'localhost'
PORT       = 5000
BASE_DIR   = os.path.abspath(os.path.dirname(__file__))

# Database Settings
DB_USER = 'adnymics'
DB_PASS = '1234'
DB_HOST = 'localhost'
DB_NAME = 'interview'

SQLALCHEMY_DATABASE_URI = 'postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}/{DB_NAME}'.format(
                            DB_USER=DB_USER,
                            DB_PASS=DB_PASS,
                            DB_HOST=DB_HOST,
                            DB_NAME=DB_NAME)
