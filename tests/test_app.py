import unittest
import json

from app import app, db

class AppTestCase(unittest.TestCase):
    """Test case for the health blueprint"""

    def setUp(self):
        """Set up test varibales"""
        self.app = app
        self.client = self.app.test_client()
        db.create_all()

    def tearDown(self):
        """Tear Down"""
        db.session.remove()
        db.drop_all()

    def test_check_health(self):
        """test the health endpoint"""
        response = self.client.get('/api/health')
        assert response.status_code in [200, 400]
        result = json.loads(response.data)
        if response.status_code == 200:
            self.assertEqual(result['message'], "running")
        if response.status_code == 400:
            self.assertEqual(result['message'], "failed")

    def test_post_order(self):
        """test with post order"""
        data = dict(
            order_id="12345678",
            products=["product_id_1", "product_id_2", "product_id_3"],
            customer_id=12345678,
            customer_name="test",
            customer_surname="user",
            customer_address="some test address",
            customer_zipcode="301311"
        )
        response = self.client.post('/api/orders', data=json.dumps(data), content_type='application/json')
        assert response.status_code == 201
        result = json.loads(response.data)
        self.assertEqual(result['order_id'], data['order_id'])
        self.assertEqual(result['products'], data['products'])

    def test_post_order_with_wrong_customer_id(self):
        """test with post order with wrong customer id format"""
        data = dict(
            order_id="12345678",
            products=["product_id_1", "product_id_2", "product_id_3"],
            customer_id=123456, # length is incorrect
            customer_name="test",
            customer_surname="user",
            customer_address="some test address",
            customer_zipcode="301311"
        )
        response = self.client.post('/api/orders', data=json.dumps(data), content_type='application/json')
        assert response.status_code == 400

    def test_post_order_with_wrong_products(self):
        """test with post order with wrong products format"""
        data = dict(
            order_id="12345678",
            products="string instead of array", # data type is incorrect
            customer_id=12345678,
            customer_name="test",
            customer_surname="user",
            customer_address="some test address",
            customer_zipcode="301311"
        )
        response = self.client.post('/api/orders', data=json.dumps(data), content_type='application/json')
        assert response.status_code == 400

    def test_get_existing_order(self):
        """get an existing order with customer id"""
        data = dict(
            order_id="12345678",
            products=["product_id_1", "product_id_2", "product_id_3"],
            customer_id=12345678,
            customer_name="test",
            customer_surname="user",
            customer_address="some test address",
            customer_zipcode="301311"
        )
        response = self.client.post('/api/orders', data=json.dumps(data), content_type='application/json')
        order = json.loads(response.data)
        response = self.client.get('/api/orders/{}'.format(order['customer_id']))
        assert response.status_code == 200
        result = json.loads(response.data)
        self.assertEqual(result['order_id'], order['order_id'])

    def test_get_not_existing_order(self):
        """get an not existing order with correct customer id format"""
        response = self.client.get('/api/orders/12345678')
        assert response.status_code == 404

    def test_get_not_existing_order_with_wrong_customer_id(self):
        """get an not existing order with incorrect customer id format"""
        response = self.client.get('/api/orders/123456789')
        assert response.status_code == 400
